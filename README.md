# Setting up Rancher, Fedex and other important components

## Requirements

  * [Docker Engine](https://docs.docker.com/engine/installation/linux/ubuntulinux/) for Ubuntu/Linux
  * [Docker Machine](https://docs.docker.com/machine/install-machine/) for Ubuntu/Linux
  * [Docker Toolbox](https://www.docker.com/products/docker-toolbox) for OS X and Windows
  * [Digital Ocean API Key](https://cloud.digitalocean.com/settings/api/tokens/new) (you need to login first)
  * [Docker Hub account](https://hub.docker.com/) tell me your username so that I can add you as collaborator to the private images

Make sure that `docker-machine` is running on your system by typing `docker-machine version`. It should output `0.7.0` or higher.

## Table of Contents

To shorten the document length I will be dividing it into modules

  1. [Rancher](rancher.md)
  2. [CI/CD using Gitlab](gitlab.md)
  3. [Logging with ELK](elk.md)
  4. [Monitoring with Prometheus](prometheus.md)
  5. [Persistent Storage with GlustrFS](gluster.md)