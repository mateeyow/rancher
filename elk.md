# Elasticsearch, Logstash, Kibana (ELK) + Logspout for Logging

For this part of the documentation we will be launching a logging application with the use of ELK-stack. I decided to separate logging with monitoring since they have their own functionalities and to fully utilize its capabilities. 

# Elasticsearch

### What is Elasticsearch?

> Elasticsearch is an open-source, broadly-distributable, readily-scalable, enterprise-grade search engine. Accessible through an extensive and elaborate API, Elasticsearch can power extremely fast searches that support your data discovery applications.

So primarily, the main goal of elastic search is to store a JSON-structured data and using it's complex algorithm should be able to query and search complex query statements extremely fast. For logging, this is where all the logs are stored.

# Logstash

### What is Logstash?

> Logstash is an open source data collection engine with real-time pipelining capabilities. Logstash can dynamically unify data from disparate sources and normalize the data into destinations of your choice. Cleanse and democratize all your data for diverse advanced downstream analytics and visualization use cases.

Logstash's main role on the stack is to filter, transform, simplify all logs received to be saved on elasticsearch.

# Kibana

### What is Kibana?

> Kibana is an open source data visualization plugin for Elasticsearch. It provides visualization capabilities on top of the content indexed on an Elasticsearch cluster. Users can create bar, line and scatter plots, or pie charts and maps on top of large volumes of data.

One word - **CHARTS**

# Logspout

### What is Logspout?

> Logspout is a log router for Docker containers that runs inside Docker. It attaches to all containers on a host, then routes their logs wherever you want. It also has an extensible module system.

To put it simply it gets logs from docker containers from the host and pass it to logstash

## Creating an elasticsearch stack
  
  1. First stop and delete the `wordpress` and `node` stack to free up port 80.
  2. On your terminal move to [files/elasticearch](files/elasticsearch) folder. 
  3. Execute the following command:

    ```bash
    rancher-compose -p es up -d
    ```
  4. You can then check on your Rancher UI that an elasticsearch stack is created. After it is created, checkout the port 80 on the `kopf` service.
    [![Elasticsearch](http://i.imgur.com/PInspEe.png)](http://i.imgur.com/PInspEe.png)
  5. On your terminal type the ff command:

    ```bash
    rancher-compose -p es scale elasticsearch-datanodes=3
    ```
  6. If you notice on the `kopf` application the number of nodes changed from 3 to 5
    [![Kopf](http://i.imgur.com/DQoOzEL.png)](http://i.imgur.com/DQoOzEL.png)

## Creating a logstash stack

  1. To run logstash move to folder [files/logstash](files/logstash) run the ff command:

    ```bash
    rancher-compose -p logstash up -d
    ```

If you inspect the [rancher-compose.yml file](files/logstash/rancher-compose.yml) you can see the logstash configuration like so:

```yml
filters: |
    grok {
       match => {
        "message" => "%{COMBINEDAPACHELOG}"
      }
    }
    geoip {
      source => "clientip"
      target => "geoip"
    }
  outputs: |
    elasticsearch {
      host => "elasticsearch"
      protocol => "http"
      index => "logstash-demo-%{+YYYY.MM.dd}"
    }
```

The `grok` part means to transform the logs that passed to logstash into a pattern `%{COMBINEDAPACHELOG}`. You can know more about grok on this [documentation](https://www.elastic.co/guide/en/logstash/current/plugins-filters-grok.html). You can play around with the configuration then create a stack using the command above.

## Create logspout stack

  1. Let's run logspout to start logging all the container's logs. Run the command:

    ```bash
    rancher-compose -p logspout up -d
    ```

If you check the kopf UI, you can see that there are some indices added. That means that our elasticsearch has some data already.
[![Index](http://i.imgur.com/XpEOVkv.png)](http://i.imgur.com/XpEOVkv.png)

## Create a kibana stack

  1. Go to [files/kibana](files/kibana) folder and execute this command:
  
    ```bash
    rancher-compose -p kibana up -d
    ```
  2. Click the port `80` of the `kibana-vip` service and it will redirect you to a kibana UI.
    [![Kibana](http://i.imgur.com/tojWzCm.png)](http://i.imgur.com/tojWzCm.png)
  3. Check `Use event times to create index names`, and change the `Input name or pattern` value to `[logstash-demo-]YYYY.MM.DD`. Then click `Create`
  4. Head to `Discover` and you can see all the logs that are saved on Elasticsearch
    [![Kibana Discover](http://i.imgur.com/0v8RqTY.png)](http://i.imgur.com/0v8RqTY.png)