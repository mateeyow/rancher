'use strict';
const http = require('http');
const app = require('./src/app');

const server = http.createServer(app);

const errorServer = error => {
  console.error(`Error in starting app ${error}`);
  process.exit(1);
};

const successServer = () => {
  console.info('Successfully started app on port: 8000');
};

server.listen(8000);
server.on('error', errorServer);
server.on('listening', successServer);
