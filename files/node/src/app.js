'use strict';
const express = require('express');
const redis = require('redis');

const app = express();

const HOST = process.env.REDIS || 'localhost';

const client = redis.createClient(6379, HOST);

app.get('/', (req, res, next) => {
  client.incr('counter', (err, counter) => {
    if (err) return next(err);

    return res.send(`This page has been viewed ${counter}! UPDATED!!`);
  });
});

app.get('/decrement', (req, res, next) => {
  client.decr('counter', (err, counter) => {
    if (err) return next(err);

    return res.send(`The counter was decremented. The count is now: ${counter}!`);
  });
});

app.get('*', (req, res, next) => res.status(404).send('404! URL not found!'));

module.exports = app;
