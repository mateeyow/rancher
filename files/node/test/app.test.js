'use strict';
const chai = require('chai');
const request = require('supertest');
const should = chai.should();

const app = require('../src/app');


describe('Simple response count', () => {
  let count;

  it('should show the number of count', done => {
    request(app)
      .get('/')
      .expect(200)
      .end((err, res) => {
        /This page has been viewed (\d+)!/g.test(res.text).should.be.true;
        count = res.text.match(/\d+/)[0];
        done();
      });
  });

  it('should decrement count', done => {
    request(app)
      .get('/decrement')
      .expect(200)
      .end((err, res) => {
        const newCount = parseInt(res.text.match(/\d+/)[0]);
        newCount.should.equal(count - 1);
        done();
      });
  });

  it('should response 404', done => {
    request(app)
      .get('/test')
      .expect(404)
      .end((err, res) => done());
  });
});
