# CI/CD Using Gitlab and Docker

For this tutorial we will be creating a CI/CD pipeline utilizing Gitlab's [Continuos Integration feature](https://about.gitlab.com/gitlab-ci/) with Docker and Rancher. We will be utilizing another application since fedex app takes a long time for build to finish. 

## Requirements

  * [Gitlab Account](https://gitlab.com/users/sign_in)
  * [Previously created Rancher](rancher.md)
  * [Docker Machine](https://docs.docker.com/machine/install-machine/) for Ubuntu/Linux
  * [Docker Hub account](https://hub.docker.com/) tell me your username so that I can add you as collaborator to the private images
  * [Docker Engine](https://docs.docker.com/engine/installation/linux/ubuntulinux/) for Ubuntu/Linux

## Create Gitlab Repository

  1. On gitlab create your own node repository.
  2. On your newly created repository, go to **Settings** > **Variables** and input the following key-value variables, just replace the value to your own environment variables
    * `DOCKER_USERNAME` = YOUR_DOCKER_USERNAME
    * `DOCKER_PASSWORD` = YOUR_DOCKER_PASSWORD
    * `RANCHER_URL` = URL_OF_YOUR_RANCHER_UI
    * `RANCHER_ACCESS_KEY` = RANCHER_ACCESS_KEY
    * `RANCHER_SECRET_KEY` = RANCHER_SECRET_KEY
  3. On this repository, initialize a git repository on `files/node`.
  4. Push the node repository to gitlab. After pushing you should see a `PENDING` build on your repoistory
    [![Gitlab](http://i.imgur.com/mde5Mpw.png)](http://i.imgur.com/mde5Mpw.png)

## Creat Gitlab CI Runners

[![asciicast](https://asciinema.org/a/3thw5j92dxle5m9eoos3ji7qq.png)](https://asciinema.org/a/3thw5j92dxle5m9eoos3ji7qq)

  1. The stage `build` would likely fail since you have no access to docker when using the default gitlab runner. Let's create our own gitlab runner. Go to **Settings** > **Runners** and copy the `Registration token`
  2. On your terminal execute `eval $(docker-machine env rancher)` and run this command:

    ```bash
    docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
    ```
  3. Make sure that it ran successfully by executing `docker ps`. Then execute `docker exec -it gitlab-runner bash`. You should be using the terminal of gitlab-runner. Then execute the following command (replace it with your own token):
  
    ```bash
    gitlab-runner register -n \
    --url https://gitlab.com/ci \
    --r tqFJykXjfyQKCkv8e4sv \
    --executor docker \
    --description "Gitlab-runner" \
    --docker-image "docker:latest" \
    --docker-privileged
    ```
  4. If there are no error, you should see a new runner on your repository (Settings > Runners)
  5. Go back to your project then go to **Builds** and click `retry` on the failed build. It should start running soon.
  6. If everything passed, you should see a new docker image on your Docker Hub account and a `node` service on your Rancher UI.

You just created your own Docker Hub image and launched an application on Rancher UI using Gitlab CI. If you check the [.gitlab-ci.yml](files/node/.gitlab-ci.yml) file, you can see three stages on your CI/CD you can play around the configuration and push it to your Gitlab repository. Check out their [documentation](http://docs.gitlab.com/ce/ci/yaml/README.html) for more information about .gitlab-ci.yml fiile.