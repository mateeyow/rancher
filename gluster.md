# Persistent Storage on Rancher

To persist all stateful container we need to create a persistent storage. For this documentation we will use [convoy](https://github.com/rancher/convoy) - a docker volume plugin and [glusterfs](https://www.gluster.org/) - a scalable network filesystem.

## Run glusterfs stack

> Only one glusterfs stack can be run per host, so you if create a gluster stack you cannot add more.

  1. Go to your Rancher UI and head to **Catalog**. Search for the word `gluster`.
    [![Gluster Search](http://i.imgur.com/5mfFkh4.png)](http://i.imgur.com/5mfFkh4.png)
  2. Click on `View Details`. Change the `Volume Name` to `persistent_volume` and click on `Launch`
    [![Gluster Stack](http://i.imgur.com/3EundeF.png)](http://i.imgur.com/3EundeF.png)
  3. Go back to **Catalog** and search for `Convoy GlusterFS`. Click on `View Details` and change the ff fields:
    * Stack Name: `convoy`
    * GlusterFS Volume: `persistent_volume`
    * Choose `glusterfs-server` on GlusterFS Server dropdown
  4. It will then be created on the `System` tab instead of `Stack`. To check if everything is good. Head to **Infrastructure** > **Storage Pools** and it should look something like this:
    [![Storage Pools](http://i.imgur.com/MVyRObo.png)](http://i.imgur.com/MVyRObo.png)

## Try out persistent storage

Remember the node docker image that you made? Let's try launching it again but on the port `8080` since all port `80` are occupied by other services.

  1. Click on `Add Stack` on **Applications** > **Stack** route.
  2. Name it node and hit on `Create`
  3. Add a service with the following input values:
    * Name: `redis`
    * Select Image: `redis:alpine`
    * Command: `--appendonly yes` (this is to save data on filesystem since redis stores it in memory only)
    * On `Volumes` tab add new volume and input the value: `redis:/data`
    * Volume Driver: `convoy` then hit on `Create`
  4. If you check on your **Storage Pools** a new volume should be added
    [![Redis Volume](http://i.imgur.com/QrdvfaT.png)](http://i.imgur.com/QrdvfaT.png)
  5. Create a new service with the ff values:
    * Name: `node`
    * Select Image: `your-user-name/node`
    * Add a `Port Map` having the ff key-value: `Public Host: 8080` `Private Container Port: 8000`
      [![Port](http://i.imgur.com/8gvUQ58.png)](http://i.imgur.com/8gvUQ58.png)
    * Add Environment Variable with key value: `REDIS=redis`. Hit on Create
      [![Env](http://i.imgur.com/lK3GWpu.png)](http://i.imgur.com/lK3GWpu.png)
  6. Visit the port `8080` on newly create `node` service and everytime you hit refresh you should have an incrementing value.
  7. Delete the redis service.
  8. Follow `step 3` **ONLY**. Visit the `node` on port `8080` again and you should see that the value stays the same.
