# Monitoring with Prometheus

Prometheus is an open-source systems monitoring and alerting originally built by SoundCloud. Since, alot of companies and organization are using Prometheus it has been decided to make it a standalone open-source project free from any company.

## Launch a prometheus stack

  1. Go to [files/prometheus folder](files/prometheus) and execute the ff command:

    ```bash
    rancher-compose -p prometheus up -d
    ```
    [![Prometheus Stach](http://i.imgur.com/UudosMI.png)](http://i.imgur.com/UudosMI.png)
  2. Check the `alertmanager` port `9093` and it should receive an alert from prometheus that `http://104.131.78.57/` is down. Since the ip address is already outdated and is not in use. 

To change the configurations, create your own docker image with the configuration and replace all `mateeyow/*-conf` text on the [docker.compose.yml](files/prometheus/docker-compose.yml) file and then run the command above. You can read more about prometheus on this [documentation](https://prometheus.io/docs/introduction/overview/)