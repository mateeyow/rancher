# Rancher

## Table of Contents

* [Introduction](#introduction)
* [Features](#features)
* [Setting up Rancher](#setting-up-rancher)
* [Setup Access Control](#setup-rancher-access-control)
* [Adding host](#adding-host)
* **OPTIONAL** [Create Wordpress App](#optional-create-wordpress-app)
* [Create the Fedex stack](#create-the-fedex-stack)

## Introduction

Rancher is an open-source software platform that implements a purpose-built infrastructure for running containers in production. Rancher provides an easy-to-use user interface to orchestrate and manage docker containers and hosts. 

## Features

  * Cross-host networking
  * Container load-balancing
  * Persistent storage services
  * Service discovery
  * Service upgrades
  * Resource management
  * Multi-tenancy & user management
  * Supports Mesos, Kubernetes and Swarm.

## Setting up Rancher

<a href="https://asciinema.org/a/2f0gkpck3gchgblksuctxkgkd" target="_blank"><img src="https://asciinema.org/a/2f0gkpck3gchgblksuctxkgkd.png" /></a>

  1. Export the DigitalOcean API Key on your terminal `export VERSUS_API_TOKEN=paste-your-digitalocean-api-token-here`
  2. Create a droplet using docker-machine

    ```bash
    docker-machine create --driver digitalocean \
      --digitalocean-size "2GB" \
      --digitalocean-region "nyc3" \
      --digitalocean-image "ubuntu-14-04-x64" \
      --digitalocean-private-networking \
      --digitalocean-access-token $VERSUS_API_TOKEN \
      rancher
    ```
  3. Set rancher droplet as your environment on your terminal

    ```bash
    eval $(docker-machine env rancher)
    ```
  4. Create rancher server container

    ```bash
    docker run -d -p 8080:8080 rancher/server:v1.0.2
    ```
  5. Make sure that rancher is done initializing by running `docker logs -f containerId`. It's complete once the logs shows `.... Startup Succeeded, Listening on port...`
  6. Visit the Rancher UI by going to the droplet ip address plus the port `8080`. To get the ip address of rancher type `docker-machine ip rancher`

## Setup Rancher Access Control

Rancher will be available to anyone who has the ip address, so we **must** setup access control. The first user that will be created will be an admin. 

[![Imgur](http://i.imgur.com/1SaOKDO.png)](http://i.imgur.com/1SaOKDO.png)

  1. To setup access control go to **Admin** > **Access Control**.
  2. Choose either Github or Local, for now let's choose Local. 
  3. Suppy information as asked.
  4. Click Enable Access Control

## Adding host

Before you can launch containers you should add hosts to your rancher server.

[![Imgur](http://i.imgur.com/yG2aJ0c.png)](http://i.imgur.com/yG2aJ0c.png)

  1. To add hosts go to **Infrastructure** > **HOSTS** then click `Add Host` button
  2. It should ask a Host Registration URL. Just click `Save`
  3. You will be then directed to a list of hosting provides. Click on the DigitalOcean logo.
  4. Fill up the required data. For this example let's boot up
    * Quantity: `3`
    * Paste your DigitalOcean access token
    * Size: `4GB`
    * Enable the `Private Networking` option
  5. Then click `Create`
  6. 3 hosts will then be created on DigitalOcean, wait for the servers to initialize. After initializing it should look like this:
    
    [![Imgur](http://i.imgur.com/7OjWSVo.png)](http://i.imgur.com/7OjWSVo.png)

## (OPTIONAL) Create Wordpress App

This is an optional exercise, you can skip this one if you like. The goal of this exercise is to have a look and feel on how to launch your container on rancher.

  1. Create a Stack. Go to **Applications** > **Stacks**.
  2. Click on `Add Stack`
  3. Name it `wordpress`, then click `Create`
  4. Let's add our first service. Click on `Add Service` and input the following data:
    * Name: `database`
    * Select Image: `mysql:latest`
    * Add Environment Variable: `MYSQL_ROOT_PASSWORD=pass1`
    * Click `Create`
  5. A new container should then be created. Let's add our wordpress service with the following data:
    * Scale: `2`
    * Name: `myblog`
    * Select Image: `wordpress:latest`
    * Click on the + on `Service Links` and select the database service, provide a name of `mysql`
    * Click `Create`
  6. Finally create a load balancer service by clicking on the dropdown button on the `Add service` and choose `Add Load Balancer`
    * Name: `loadbalancer`
    * Add both Source IP/Port and Default Target Port with `80`
    * Select `myblog` as your Target Service
    * Hit on `Save`
  7. After all services are done creating, you should see a link on port 80 of the load balancer service. Click on that link, if everything went well it should direct you to a wordpress application.  

The stack should more or less look like this: 

[![Imgur](http://i.imgur.com/kzeSKgY.png)](http://i.imgur.com/kzeSKgY.png)

## Create the Fedex stack

There are two ways on launching containers on rancher. First is through UI - same as creating the wordpress application above. For the second one it's through your terminal. We will be tackling that one and we will create a fedex app through rancher.

  1. Create access key by going to **API**
  2. Click on `Add Environment API Key` and fill out the name.
  3. Copy the access key and secret key on a secured location.
  4. On your terminal export the following environment variables

    ```bash
    export RANCHER_URL=RANCHER_IP_ADDRESS:8080
    export RANCHER_ACCESS_KEY=pasteyouraccesskeyhere
    export RANCHER_SECRET_KEY=pasteyoursecretkeyhere
    ```
  5. Add your registry account on Rancher UI by going to **Infrastructure** > **Registries**. Click on `Add Registry`
  6. Choose `Docker Hub` as Registry Provider and supply your credentials. Make sure to inform me on your username so that you can download the private images.
  7. Download the rancher-compose cli by clicking the download link on the lower right of your Rancher UI
    [![Rancher Compose](http://i.imgur.com/Bjc8g16.png)](http://i.imgur.com/Bjc8g16.png)
  8. Extract the downloaded file and go to the folder, you should see a `rancher-compose` executable file.
  9. Move the downloaded file to your `/usr/local/bin` folder.
  10. On this repository go to files/rancher-fedex, you should see [docker-compose.yml](files/rancher-fedex/docker-compose.yml) and [rancher-compose.yml](files/rancher-fedex/rancher-compose.yml)
  11. RANCHER_URL, RANCHER_ACCESS_KEY, and RANCHER_SECRET_KEY should be on your environment variable. Execute the following bash command:

    ```bash
    rancher-compose -p fedex up -d
    ```
  12. After everything is up you should see a new stack called `fedex` on your Rancher UI. It should more or less look like this
    [![Fedex Stack](http://i.imgur.com/wmvw0Ew.png)](http://i.imgur.com/wmvw0Ew.png)
  13. Click on the port 80 link to go to the versus application
